
<?php
	$a = "Hello ";
	$b = $a . "World!";
	echo $a.$b;
	echo "<br>";

	$a = "Hello ";
	$a .= "World!";
	echo $a;
	echo "<br>";     
?>

<?php

	$var = "hello";
	$world = "world";

	echo "$var" . '$world';
	echo "<br>";

	echo "$var" . "$world"; 
	echo "<br>";

	echo "$var" .  $world;
	echo "<br>";

?>
