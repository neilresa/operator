<?php
	echo "<h3>Postincrement</h3>";
	$a = 5;
	echo $a++ . "<br>";
	echo $a . "<br>";

	echo "<h3>Preincrement</h3>";
	$a = 5;
	echo ++$a . "<br>";
	echo $a . "<br>";

	echo "<h3>Postdecrement</h3>";
	$a = 5;
	echo $a-- . "<br>";
	echo $a . "<br>";

	echo "<h3>Predecrement</h3>";
	$a = 5;
	echo --$a . "<br>";
	echo $a . "<br>";
?>
